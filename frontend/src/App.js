import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './components/screens/dashboard';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const Root = () => {
    return <Dashboard/>;
};

let container = document.getElementById('app');
let component = <Root/>;
ReactDOM.render(component, container);