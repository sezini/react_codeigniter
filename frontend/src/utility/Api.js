import axios from 'axios';

const API = axios.create({
    baseURL: 'http://localhost:8080',
    responseType: 'json',
});

const getRequestConfiguration = () => {
    const headers = {
        'Content-Type': 'application/json',
    };
    return { headers };
};

export const makeRequest = ({
                                url,
                                values,
                                successCallback,
                                successFileCallback,
                                successGetFileCallback,
                                failureCallback,
                                requestType = 'POST',
                            }) => {
    
    const requestConfiguration = getRequestConfiguration();
    let promise;

    switch (requestType) {
        case 'GET':
            promise = API.get(url, requestConfiguration);
            break;
        case 'POST':
            promise = API.post(url, values, requestConfiguration);
            break;
        case 'FILE':
            promise = API.post(url, values, {
                headers: {
                  'Content-Type': 'multipart/form-data',
                }
            });
            break;
        case 'DELETE':
            promise = API.delete(url, requestConfiguration);
            break;
        default:
            return;
    }
   
    promise
        .then((response) => {
            const { data } = response;
            if (data.arquivo) {
                successFileCallback(data)
            } else {
                successCallback(data);
            }
        })
        .catch((error) => {
            if (error.response) {
                failureCallback(error.response.data);
            }
        });
};