import React from 'react';
import DashboardMenu from './Menu';
import {BrowserRouter as Router} from 'react-router-dom';
import Routes from '../../routing/Routes';

const Dashboard = () => {

    return (
        <Router>
            <>
                <DashboardMenu/>
                <div style={{ margin: '60px' }}>
                    <Routes/>
                </div>
            </>
        </Router>
    )
};

export default Dashboard;