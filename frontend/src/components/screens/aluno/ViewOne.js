import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardText,
    CardTitle,
} from 'reactstrap';
import LoadingAlert from '../../alert/Loading';
import { makeRequest } from '../../../utility/Api';
import { Redirect } from 'react-router-dom';

const AlunoView = () => {
    const { id } = useParams();
    const [dadosCarregados, setDadosCarregados] = useState(false);
    const [alunoExibir, setAlunoEditar] = useState(false);
    const [shouldRedirect, setShouldRedirect] = useState(false);

    useEffect(() => {
        carregaAluno(id);
    }, []);

    const carregaAluno = (id_aluno) => {
        makeRequest({
            url: `alunos/getById/${id_aluno}`,
            successCallback: (data) => {
                const {aluno} = data;
                setAlunoEditar(aluno);
                setDadosCarregados(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
        });
    };

    return shouldRedirect ? (
        <Redirect to='/' />
    ) : (
        dadosCarregados ? (
            <div className='centerDiv' style={{ marginTop: '50px' }}>
                <Card style={{ textAlign: 'center' }}>
                    <CardHeader>{alunoExibir.nome}</CardHeader>
                    <CardBody>
                        <CardTitle>
                            Endereço
                        </CardTitle>
                        <CardText>Logradouro: {alunoExibir.logradouro}</CardText>
                        <CardText>Número: {alunoExibir.numero}</CardText>
                        <CardText>Complemento: {alunoExibir.complemento}</CardText>
                        <CardText>Bairro: {alunoExibir.bairro}</CardText>
                        <CardText>Cidade: {alunoExibir.cidade}</CardText>
                        <CardText>Estado: {alunoExibir.estado}</CardText>
                        <CardText>Cep: {alunoExibir.cep}</CardText>
                        <div className='centerImage'>
                            {alunoExibir.caminhoArquivo ?
                                (
                                    <div>
                                        <img className='centerImage' src={`http://localhost:8080/${alunoExibir.caminhoArquivo}`} alt="imagem Aluno"></img>
                                    </div>
                                ) : null
                            }
                        </div>
                    </CardBody>
                    <CardFooter style={{ textAlign: 'left' }}>
                        <Button onClick={setShouldRedirect}>Voltar</Button>
                    </CardFooter>
                </Card>
            </div>
        ) : (
            <LoadingAlert/>
        )
    );
};

export default AlunoView;