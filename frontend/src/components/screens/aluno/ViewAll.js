import React, {useEffect, useState} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, Table, UncontrolledButtonDropdown,} from 'reactstrap';
import {makeRequest} from '../../../utility/Api';
import SuccessAlert from '../../alert/Success';
import LoadingAlert from '../../alert/Loading';
import {Link} from 'react-router-dom';

const AlunosTable = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [alunosExibicao, setAlunos] = useState([]);
    const [responseMessage, setResponseMessage] = useState('');
    const [showSuccessAlert, setShowSuccessAlert] = useState(false);

    const onTimeout = () => {
        setShowSuccessAlert(false);
    };

    const deleteAluno = (alunoId) => {
        makeRequest({
            url: `alunos/delete/${alunoId}`,
            successCallback: (data) => {
                setResponseMessage(data.message);
                carregaAlunos();
                setShowSuccessAlert(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
        });
    };

    const carregaAlunos = () => {
        makeRequest({
            url: 'alunos/getAll',
            successCallback: (data) => {
                const {alunos} = data;
                setAlunos(alunos);
            },
            requestType: 'GET',
        });
        setIsLoading(false);
    };

    useEffect(() => {
        carregaAlunos();
    }, []);

    return isLoading ? (
        <LoadingAlert/>
    ) : (
        <>
            {showSuccessAlert && (
                <SuccessAlert {...{message: responseMessage, onTimeout}} />
            )}
            <div style={{textAlign: 'center', margin: '20px'}}>
                <h1>Alunos Cadastrados</h1>
            </div>
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th style={{width: '40%'}}>Nome</th>
                    <th style={{width: '20%'}}>Cidade</th>
                    <th style={{width: '20%'}}>Estado</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                {alunosExibicao?.map((aluno, index) => (
                    <tr key={aluno.id_aluno}>
                        <th scope='row'>{index + 1}</th>
                        <td>{aluno.nome}</td>
                        <td>{aluno.cidade}</td>
                        <td>{aluno.estado}</td>
                        <td>
                            <UncontrolledButtonDropdown>
                                <DropdownToggle caret>Opções</DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem>
                                        <NavLink
                                            tag={Link}
                                            to={`/aluno/view/${aluno.id_aluno}`}>
                                            Visualizar
                                        </NavLink>
                                    </DropdownItem>
                                    <DropdownItem divider/>
                                    <DropdownItem>
                                        <NavLink
                                            tag={Link}
                                            to={`/aluno/edit/${aluno.id_aluno}`}>
                                            Editar
                                        </NavLink>
                                    </DropdownItem>
                                    <DropdownItem divider/>
                                    <DropdownItem
                                        onClick={() => {
                                            deleteAluno(aluno.id_aluno);
                                        }}
                                    >
                                        <NavLink>
                                            Delete
                                        </NavLink>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </>
    );
};

export default AlunosTable;