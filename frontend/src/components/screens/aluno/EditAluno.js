import React, { useState, useEffect } from 'react';
import BaseAlunoForm from './BaseAlunoForm';
import { useParams } from 'react-router-dom';
import { makeRequest } from '../../../utility/Api';
import LoadingAlert from '../../alert/Loading';

const EditAlunoForm = () => {
    const { id } = useParams();
    const [dadosCarregados, setDadosCarregados] = useState(false);
    const [alunoEditar, setAlunoEditar] = useState(false);

    useEffect(() => {
        carregaAluno(id);
    }, []);

    const carregaAluno = (id_aluno) => {
        makeRequest({
            url: `alunos/getById/${id_aluno}`,
            successCallback: (data) => {
                const {aluno} = data;
                setAlunoEditar(aluno);
                setDadosCarregados(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
        });
    };
    return dadosCarregados ? (
        <BaseAlunoForm aluno={alunoEditar} />
    ) : (
        <LoadingAlert/>
    )
};

export default EditAlunoForm;