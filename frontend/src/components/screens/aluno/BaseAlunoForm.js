import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import { Button, FormGroup } from 'reactstrap';
import CustomInput from '../../shared/CustomInput';
import FileInput from '../../shared/FileInput';
import FailureAlert from '../../alert/Failure';
import { makeRequest } from '../../../utility/Api';
import SuccessAlert from '../../alert/Success';
import { Redirect } from 'react-router-dom';

const BaseAlunoForm = ({aluno}) => {
    const [errors, setErrors] = useState(null);
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [responseMessage, setResponseMessage] = useState('');
    const [showSuccessAlert, setShowSuccessAlert] = useState(false);
    const [arquivoSalvo, setArquivo] = useState({});
    const [arquivoExibir, setArquivoExibir] = useState({});
    const [formEnviado, setformEnviado] = useState('');

    const onTimeout = () => {
        if (formEnviado) {
            setShouldRedirect(true);
        } else {
            setShowSuccessAlert(false);
        }
    };

    const initialValues = {
        nome: aluno?.nome || '',
        id_endereco: aluno?.id_endereco || '',
        logradouro: aluno?.logradouro || '',
        numero: aluno?.numero || '',
        complemento: aluno?.complemento || '',
        bairro: aluno?.bairro || '',
        estado: aluno?.estado || '',
        cidade: aluno?.cidade || '',
        cep: aluno?.cep || '',
        id_arquivo: aluno?.id_arquivo || '',
        nomeArquivo: aluno?.nomeArquivo || '',
        caminhoArquivo: aluno?.caminhoArquivo || '',
    };

    const validationSchema = Yup.object({
        nome: Yup.string()
            .max(100, 'Nome não pode ser maior que 100 charecters.')
            .required('Nome é obrigatorio.'),
        logradouro: Yup.string()
            .max(100, 'Nome não pode ser maior que 100 charecters.')
            .required('Nome é obrigatorio.'),
        numero: Yup.number()
            .positive()
            .integer()
            .required('Numero é obrigatorio.'),
        complemento:  Yup.string()
            .max(100, 'Complemento não pode ser maior que 100 charecters.')
            .required('Complemento é obrigatorio.'),
        bairro:  Yup.string()
            .max(100, 'Bairro não pode ser maior que 100 charecters.')
            .required('Bairro é obrigatorio.'),
        estado:  Yup.string()
            .max(100, 'Estado não pode ser maior que 100 charecters.')
            .required('Estado é obrigatorio.'),
        cidade:  Yup.string()
            .max(100, 'Cidade não pode ser maior que 100 charecters.')
            .required('Cidade é obrigatorio.'),
        cep: Yup.string()
            .max(10, 'CEP não pode ser maior que 10 charecters.')
            .required('CEP é obrigatorio.'),
    });

    const uploadArquivo = (file) => {
        const formData = new FormData();
        formData.append('imagem', file);
        makeRequest({
            url: 'arquivos/save',
            values: formData,
            requestType: 'FILE',
            successFileCallback,
            failureCallback: (error) => {
                setErrors(error);
            },
        });
    };

    const successCallback = (data) => {
        setformEnviado(true);
        const { message } = data;
        setResponseMessage(message);
        setShowSuccessAlert(true);
    };

    const successFileCallback = (data) => {
        const { message , arquivo} = data;
        setResponseMessage(message);
        setShowSuccessAlert(true);
        setArquivo(arquivo);
    };

    const submitCallback = (values) => {
        values.id_arquivo = arquivoSalvo?.id_arquivo || '';
        makeRequest({
            url: `alunos${aluno ? `/update/${aluno.id_aluno}` : '/save'}`,
            values,
            successCallback,
            failureCallback: (error) => {
                setErrors(error);
            },
        });
    };

    return shouldRedirect ? (
        <Redirect to='/' />
    ) : (
        <>
            {errors && <FailureAlert errors={errors} />}
            {showSuccessAlert && (
                <SuccessAlert
                    {...{
                        message: responseMessage,
                        onTimeout,
                        shouldShow: showSuccessAlert,
                    }}
                />
            )}

            <div className='centerDiv'>
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={submitCallback}
                >
                    <Form>
                        <FormGroup row>
                            <CustomInput name={'nome'} label={'Nome'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'logradouro'} label={'Logradouro'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput
                                name={'numero'}
                                label={'Número'}
                                type={'number'}
                            />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'complemento'} label={'Complemento'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'bairro'} label={'Bairro'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'cidade'} label={'Cidade'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'estado'} label={'Estado'} />
                        </FormGroup>
                        <FormGroup row>
                            <CustomInput name={'cep'} label={'Cep'} />
                        </FormGroup>
                        <FormGroup>
                            <FileInput
                                name={'imagem'}
                                label={'tipos aceitaveis: JPG | Tamanho maximo: 5mb'}
                                returnedFile={uploadArquivo}
                            />
                        </FormGroup>
                        <FormGroup row className='centerImage'>
                            {arquivoSalvo.caminhoArquivo ?
                                (
                                    <div>
                                        <img className='centerImage' src={`http://localhost:8080/${arquivoSalvo.caminhoArquivo}`} alt="imagem Aluno"></img>
                                    </div>
                                ) : null
                            }
                            {aluno?.caminhoArquivo && !arquivoSalvo.caminhoArquivo ?
                                (
                                    <div>
                                        <img className='centerImage' src={`http://localhost:8080/${aluno.caminhoArquivo}`} alt="imagem Aluno"></img>
                                    </div>
                                ) : null
                            }
                        </FormGroup>
                        <Button type='submit' color={'danger'} onClick={setShouldRedirect}>
                            Cancelar
                        </Button>
                        {aluno ? 
                            (
                                <Button className='buttonAdd' type='submit' color={'success'}>
                                    Editar
                                </Button>
                            ) : (
                                <Button className='buttonAdd' type='submit' color={'success'}>
                                    Adicionar
                                </Button>
                            )
                        }
                    </Form>
                </Formik>
            </div>
        </>
    );
};

export default BaseAlunoForm;