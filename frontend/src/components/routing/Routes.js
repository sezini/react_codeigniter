import React from 'react';
import {Route, Switch} from 'react-router-dom';
import AlunosTable from '../screens/aluno/ViewAll';
import PageNotFound from '../alert/PageNotFound';
import AlunoView from '../screens/aluno/ViewOne';
import AddAlunoForm from '../screens/aluno/AddAluno';
import EditAlunoForm from '../screens/aluno/EditAluno';

const Routes = () => (
    <Switch>
        <Route path='/' exact component={AlunosTable}/>
        <Route path={'/aluno/view/:id'} component={AlunoView}/>
        <Route path={'/aluno/add'} component={AddAlunoForm}/>
        <Route path={'/aluno/edit/:id'} component={EditAlunoForm}/>
        <Route path={'*'} component={PageNotFound}/>
    </Switch>
);
export default Routes;