import React from 'react';
import {UncontrolledAlert} from 'reactstrap';

const FailureAlert = ({errors}) => {
    return (
        <div style={{marginTop: '-50px', position: 'absolute', right: '10px'}}>
            <UncontrolledAlert color='danger'>
                <h4 className='alert-heading'>Request Failed</h4>
                <hr/>
                <ul className='plainList'>
                    {Object.values(errors).map((error, index) => {
                        return <li key={index}>{error}</li>
                    })}
                </ul>
            </UncontrolledAlert>
        </div>
    )
}

export default FailureAlert;