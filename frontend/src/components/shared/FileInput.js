import React from 'react';
import {ErrorMessage, Field} from 'formik';
import ImageUploader from 'react-images-upload';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};
const CustomInput = ({name, label, fileTypes = ['.jpg'], returnedFile}) => {
    const getfile = (part, jogaFora) => {
        returnedFile(part[0]);
    };

    return (<>
        <Field name={name}>
            {({field}) => {
                return (<>
                    <ImageUploader
                        name={name}
                        withIcon={true}
                        buttonText='Selecione a imagem'
                        onChange={getfile}
                        imgExtension={fileTypes}
                        maxFileSize={5242880}
                        label={label}
                        singleImage={true}
                    />
                    <ErrorMessage
                        name={name}
                        render={getErrorDiv}
                    />
                </>)
            }}
        </Field>
    </>);
};

export default CustomInput;