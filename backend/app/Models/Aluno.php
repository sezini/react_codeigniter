<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;
use App\Models\Endereco;
Use App\Models\Arquivo;

class Aluno extends Model
{
    protected $table = 'aluno';
    protected $allowedFields = [
        'nome',
        'id_endereco',
        'id_arquivo',
    ];

    public function saveAluno($data) {
        $endereco = new Endereco();
        $endereco->save($data);
        $data['id_endereco'] = $endereco->getInsertID();
        $this->save($data);
        $idAluno = $this->getInsertID();
        return $this->findAlunoById($idAluno);
    }

    public function updateAluno($id, $data) {
        $endereco = new Endereco();
        $endereco->where(['id_endereco' => $data['id_endereco']])
            ->set($data)
            ->update();
        $this->where(['id_aluno' => $id])
            ->set($data)
            ->update();
        return $this->findAlunoById($id);
    }
                                      
    public function findAlunoById($id)
    {
        $aluno = $this
            ->asArray()
            ->where(['aluno.id_aluno' => $id])
            ->join('endereco', 'endereco.id_endereco = aluno.id_endereco')
            ->join('arquivo', 'arquivo.id_arquivo = aluno.id_arquivo','LEFT')
            ->first();

        if (!$aluno) {
            throw new Exception('Nenhum aluno encontrado para o id informado.');
        }

        return $aluno;
    }

    public function getAll()
    {
        $alunos = $this
            ->join('endereco', 'endereco.id_endereco = aluno.id_endereco')
            ->join('arquivo', 'arquivo.id_arquivo = aluno.id_arquivo','LEFT')
            ->findAll();

        if (!$alunos) {
            throw new Exception('Nenhum aluno cadastrado.');
        }

        return $alunos;
    }

    public function deleteById($id_aluno)
    {
        $aluno = $this
            ->where(['aluno.id_aluno' => $id_aluno])
            ->delete();

        if (!$aluno) {
            throw new Exception('Nenhum aluno encontrado.');
        }

        return $aluno;
    }
}