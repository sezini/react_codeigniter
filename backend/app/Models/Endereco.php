<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class Endereco extends Model
{
    protected $table = 'endereco';
    protected $allowedFields = [
        'logradouro',
		'numero',
		'complemento',
		'bairro',
		'estado',
		'cidade',
		'cep',
    ];
                                      
    public function findEnderecoById($id)
    {
        $endereco = $this
            ->asArray()
            ->where(['id_endereco' => $id])
            ->first();

        if (!$endereco) {
            throw new Exception('Nenhum endereco encontrado para o id informado.');
        }

        return $endereco;
    }
}