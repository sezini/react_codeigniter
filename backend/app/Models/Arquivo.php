<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class Arquivo extends Model
{
    protected $table = 'arquivo';
    protected $allowedFields = [
        'nomeArquivo',
        'caminhoArquivo',
    ];

                                      
    public function findArquivoById($id)
    {
        $arquivo = $this
            ->asArray()
            ->where(['id_arquivo' => $id])
            ->first();

        if (!$arquivo) {
            throw new Exception('Nenhum arquivo encontrado para o id informado.');
        }

        return $arquivo;
    }
}