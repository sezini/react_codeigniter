<?php

namespace App\Controllers;

use App\Models\Arquivo;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Files\File;
use Exception;

class Arquivos extends BaseController
{
    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Auth-Token');
        header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
    }

    public function index()
    {
        $modelArquivo = new Arquivo();
        return $this->getResponse(
            [
                'message' => 'Arquivos encontrados com sucesso.',
                'arquivos' => $modelArquivo->findAll()
            ]
        );
    }

    public function save()
    {
        $requiredMax100 = 'required|max_length[100]';
        $rules = [
            'nomeArquivo' => $requiredMax100,
            'caminhoArquivo' => $requiredMax100,
        ];

        $arquivoInput = [];
        $imagefile = $this->request->getFile('imagem');
        if($imagefile)
        {
            $newName = $imagefile->getRandomName();
            $arquivoInput['nomeArquivo'] = $imagefile->getName();
            $imagefile->move(ROOTPATH.'public/', $newName);
            $arquivoInput['caminhoArquivo'] = $newName;
        }

        if (!$this->validateRequest($arquivoInput, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $modelArquivo = new Arquivo();
        $modelArquivo->save($arquivoInput);
        $idArquivo = $modelArquivo->getInsertID();
        $arquivoSalvo = $modelArquivo->findArquivoById($idArquivo);

        return $this->getResponse(
            [
                'message' => 'Arquivo adicionado com sucesso.',
                'arquivo' => $arquivoSalvo
            ]
        );
    }

    public function getById($id)
    {
        try {

            $modelArquivo = new Arquivo();
            $arquivo = $modelArquivo->findArquivoById(intval($id));
            return $this->getResponse(
                [
                    'message' => 'Arquivo encontrado com sucesso.',
                    'arquivo' => $arquivo['caminhoArquivo']
                ]
            )->download(new File($arquivo['caminhoArquivo']));

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'Não foi possivel encontrar arquivo com o Id fornecido.'
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function update($id)
    {
        try {

            $modelArquivo = new Arquivo();
            $modelArquivo->findArquivoById($id);

            $input = $this->getRequestInput($this->request);

            $modelArquivo->update($id, $input);
            $arquivo = $modelArquivo->findArquivoById($id);

            return $this->getResponse(
                [
                    'message' => 'Arquivo atualizado com sucesso.',
                    'arquivo' => $arquivo
                ]
            );

        } catch (Exception $exception) {

            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function delete($id)
    {
        try {

            $modelArquivo = new Arquivo();
            $arquivo = $modelArquivo->findArquivoById($id);
            if (!$arquivo) {
                throw new Exception('Nenhum arquivo encontrado para o id informado.');
            }
            $modelArquivo->delete($id);

            return $this
                ->getResponse(
                    [
                        'message' => 'Arquivo deletedo com sucesso.',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}