<?php

namespace App\Controllers;

use App\Models\Aluno;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class Alunos extends BaseController
{
    public function index()
    {
        $modelAluno = new Aluno();
        return $this->getResponse(
            [
                'message' => 'Alunos encontrados com sucesso.',
                'alunos' => $modelAluno->findAll()
            ]
        );
    }

    public function save()
    {
        $requiredMax100 = 'required|max_length[100]';
        $rules = [
            'nome' => $requiredMax100,
            'logradouro' => $requiredMax100,
            'numero' => 'required',
            'bairro' => $requiredMax100,
            'estado' => $requiredMax100,
            'cidade' => $requiredMax100,
            'cep' => 'required|max_length[10]',
        ];

        $input = $this->getRequestInput($this->request);
        $input['id_endereco'] = intval($input['id_endereco']);
        $input['id_arquivo'] = intval($input['id_arquivo']);
        
        if ($input['id_arquivo'] == 0) {
            $input['id_arquivo'] = null;
        }

        if (!$this->validateRequest($input, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $modelAluno = new Aluno();
        $alunoSalvo = $modelAluno->saveAluno($input);

        return $this->getResponse(
            [
                'message' => 'Aluno adicionado com sucesso.',
                'aluno' => $alunoSalvo
            ]
        );
    }

    public function getById($id_aluno)
    {
        try {

            $modelAluno = new Aluno();
            $aluno = $modelAluno->findAlunoById(intval($id_aluno));

            return $this->getResponse(
                [
                    'message' => 'Aluno encontrado com sucesso.',
                    'aluno' => $aluno
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => $e->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function getAll()
    {
        try {

            $modelAluno = new Aluno();
            $aluno = $modelAluno->getAll();

            return $this->getResponse(
                [
                    'message' => 'Aluno encontrado com sucesso.',
                    'alunos' => $aluno
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => $e->getMessage()
                ],
            );
        }
    }

    public function update($id)
    {
        try {

            $modelAluno = new Aluno();

            $input = $this->getRequestInput($this->request);
            $input['id_endereco'] = intval($input['id_endereco']);
            $input['id_arquivo'] = intval($input['id_arquivo']);
            
            if ($input['id_arquivo'] == 0) {
                $input['id_arquivo'] = null;
            }

            $aluno = $modelAluno->updateAluno($id, $input);

            return $this->getResponse(
                [
                    'message' => 'Aluno atualizado com sucesso.',
                    'aluno' => $aluno
                ]
            );

        } catch (Exception $exception) {

            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function delete($id_aluno)
    {
        try {

            $modelAluno = new Aluno();
            $aluno = $modelAluno->findAlunoById(intval($id_aluno));
            if (!$aluno) {
                throw new Exception('Nenhum aluno encontrado para o id informado.');
            }
            $modelAluno->deleteById(intval($id_aluno));

            return $this
                ->getResponse(
                    [
                        'message' => 'Aluno deletedo com sucesso.',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}