<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddArquivo extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id_arquivo' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'nomeArquivo' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'caminhoArquivo' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
        ]);
        $this->forge->addPrimaryKey('id_arquivo');
        $this->forge->createTable('arquivo');
    }

    public function down()
    {
        $this->forge->dropTable('arquivo');
    }
}