<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddAluno extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id_aluno' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'nome' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
        ]);
        $this->forge->addPrimaryKey('id_aluno');
        $this->forge->createTable('aluno');
        $this->forge->addColumn('aluno',[
			'COLUMN id_endereco INT NOT NULL',
			'CONSTRAINT fk_id_endereco FOREIGN KEY(id_endereco) REFERENCES endereco(id_endereco) ON DELETE CASCADE',
		]);
		$this->forge->addColumn('aluno',[
			'COLUMN id_arquivo INT NULL',
			'CONSTRAINT fk_id_arquivo FOREIGN KEY(id_arquivo) REFERENCES arquivo(id_arquivo) ON DELETE CASCADE',
		]);
    }

    public function down()
    {
        $this->forge->dropTable('aluno');
    }
}
