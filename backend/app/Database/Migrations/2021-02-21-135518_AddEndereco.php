<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddEndereco extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id_endereco' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'logradouro' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'numero' => [
                'type' => 'INT',
                'constraint' => 5,
                'null' => false
            ],
            'complemento' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'bairro' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'estado' => [
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => false
            ],
            'cidade' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => false
            ],
            'cep' => [
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => false
            ],
        ]);
        $this->forge->addPrimaryKey('id_endereco');
        $this->forge->createTable('endereco');
    }

    public function down()
    {
        $this->forge->dropTable('endereco');
    }
}
